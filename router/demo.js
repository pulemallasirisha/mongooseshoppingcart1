const Group1 = require('../models/Groups');
const Ga = require('../models/Categories');
const pro = require('../models/products');
const express = require("express");
var router = express.Router();

//creating Groups
router.post('/groups', (req, res) => {
    const bla = new Group1(req.body);
    bla.save().then((bla) => {
        res.status(201).send(bla);
    }).catch((error) => {
        res.status(400).send(error);
    })
})


router.get('/groups', (req, res) => {
    Group1.find().then((bla) => {
        console.log(bla);
        res.send(bla);
    }).catch((error) => {
        res.status(500).send(error);
    })
})



// creating category
router.post('/category', (req, res) => {
    const cat = new Ga(req.body);
    cat.save().then((cat) => {
        res.status(201).send(cat);
    }).catch((error) => {
        res.status(400).send(error);
    })
})

router.get('/category', (req, res) => {
    Ga.find({id:req.params._id}).then((cat) => {
        res.send(cat);
    }).catch((error) => {
        res.status(500).send(error);
    })
})


// creating products
router.post('/product', (req, res) => {
    const cat = new pro(req.body);
    cat.save().then((cat) => {
        res.status(201).send(cat);
    }).catch((error) => {
        res.status(400).send(error);
    })
})



router.get('/product', (req, res) => {
    pro.find({id:req.params._id}).then((cat) => {
        res.send(cat);
    }).catch((error) => {
        res.status(500).send(error);
    })
})


module.exports = router;
