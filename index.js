require('./db/mongoose');
const express = require('express');
var groups = require('./router/demo.js');
const app = express();
app.use(express.json());
app.use('/api',groups)
app.listen(3000, (req, res) => {
    console.log('app is running in port 3000!');
})

module.exports = app;
